<!DOCTYPE html>
<html>
<head>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="libs/jquery/3.6.3/jquery.min.js"></script>
<script src="histograms.js"></script>
</head>

<body>
<h1>Sensor</h1>
<div>
  <div>
  <a href="/">Home</a>
  </div>
  <div>
  Time period 
  <input type="radio" name="db_from" value="db_from_all" id="db_from_all"><label for="db_from_all">All</label>
  <input type="radio" name="db_from" value="db_from_month" id="db_from_month"><label for="db_from_month">Last month</label>
  <input type="radio" name="db_from" value="db_from_week" id="db_from_week"><label for="db_from_week">Last week</label>
  <input type="radio" name="db_from" value="db_from_day" id="db_from_day"><label for="db_from_day">Last day</label>
  <input type="radio" name="db_from" value="db_from_hours" id="db_from_hours" checked><label for="db_from_hours">Last few hours</label>
  </div>
  <div>
  Sensors
  <select name="db_sensors" id="db_sensors"></select>
  </div>
  <button id="db_submit">Submit</button>
</div>

<div>
  <canvas id="sensor"></canvas>
</div>

<script>

function load_sensors(){
  let urlParams = new URLSearchParams(window.location.search);
  let sensor_id = (urlParams.has("sensor_id")?urlParams.get("sensor_id"):"351358811387312");
  $.ajax({
    url: 'https://sacaqm.web.cern.ch/dbread.php',
    type: 'get',
    data: {cmd:"get_sensors"},
    success: function(data) {
      //console.log(data);  
      sensors=JSON.parse(data);
      opt="";
      for(sensor of sensors){
        checked=(sensor_id==sensor["sensor_id"]?" selected ":"");
        opt+="<option "+checked+"value='"+sensor["sensor_id"]+"'>"+sensor["sensor_id"]+"</option>\n"; 
      }
      $("#db_sensors").html(opt);
    }
  }); 
}

function formatDate(date){
  return date.getFullYear()+"-"+(date.getMonth()+1<10?"0":"")+(date.getMonth()+1)+"-"+(date.getDate()<10?"0":"")+date.getDate();
}

$("#db_submit").click(request_data);
  
  
function request_data() {
  var request={cmd:"get_sen55"};
  time=$('input[name=db_from]:checked').val();
  if (time.includes("month")){
    var date = new Date();
    date.setDate(date.getDate()-30);
    request["from"]=formatDate(date);
  }else if (time.includes("week")){
    var date = new Date();
    date.setDate(date.getDate()-7);
    request["from"]=formatDate(date);
  }else if (time.includes("day")){
    var date = new Date();
    date.setDate(date.getDate()-1);
    request["from"]=formatDate(date);
  }else if (time.includes("hours")){
    var date = new Date();
    date.setDate(date.getDate()-0.5);
    request["from"]=formatDate(date);
    request["from"]+=" "+date.getHours()+":"+(date.getMinutes()<10?"0":"")+date.getMinutes()+":"+(date.getSeconds()<10?"0":"")+date.getSeconds();
  }
  for(i=0;i<$('#db_sensors').find(":selected").length;i++){
    request["sensor_id"]=$('#db_sensors').find(":selected")[i].value;
  }
  
  get_data(request);
  return false;
}

var c_sensor=null;

function get_data(request){
  $.ajax({
    url: 'https://sacaqm.web.cern.ch/dbread.php',
    type: 'get',
    data: request,
    success: function(data) {
      //console.log(data);  
      points=JSON.parse(data);
      dates = []
      pm1p0 = []
      pm2p5 = []
      pm4p0 = []
      pm10p0 = []
      humidity = []
      temperature = []
      for (p of points){ 
        dates.push(p["timestamp"]);
        date=new Date();
        date.setFullYear(parseInt(p["timestamp"].split(" ")[0].split("-")[0]));
        date.setMonth(parseInt(p["timestamp"].split(" ")[0].split("-")[1])-1);
        date.setDate(parseInt(p["timestamp"].split(" ")[0].split("-")[2]));
        date.setHours(parseInt(p["timestamp"].split(" ")[1].split(":")[0]));
        date.setMinutes(parseInt(p["timestamp"].split(" ")[1].split(":")[1]));
        date.setSeconds(parseInt(p["timestamp"].split(" ")[1].split(":")[2]));
        pm1p0.push({x:date.getTime(),y:parseFloat(p["pm1p0"])});
        pm2p5.push({x:date.getTime(),y:parseFloat(p["pm2p5"])});
        pm4p0.push({x:date.getTime(),y:parseFloat(p["pm4p0"])});
        pm10p0.push({x:date.getTime(),y:parseFloat(p["pm10p0"])});
        humidity.push({x:date.getTime(),y:parseFloat(p["humidity"])});
        temperature.push({x:date.getTime(),y:parseFloat(p["temperature"])});
      }
      
      if (c_sensor) {c_sensor.clear();c_sensor.destroy();c_sensor=null;}
      c_sensor = new Chart($("#sensor"), {
          type: 'line',
          data: {
            labels: dates,
            datasets: [
              {label: 'pm1.0', data: pm1p0, showLine: false},
              {label: 'pm2.0', data: pm2p5, showLine: false},
              {label: 'pm4.5', data: pm4p0, showLine: false},
              {label: 'pm10.0', data: pm10p0, showLine: false},
              {label: 'humidity', data: humidity, showLine: false, yAxisID: 'yh',},
              {label: 'temperature', data: temperature, showLine: false, yAxisID: 'yt'},
            ]
          },
          options: { 
            scales: { 
              x: { title: { text: 'Time' , display: true }}, //type: 'time', time: {unit: 'millisecond'}}
              y: { title: { text: 'PPM', display: true }},
              yt: { title: { text: 'C', display: true }, position:'right', grid: { drawOnChartArea: false}},
              yh: { title: { text: '%', display: true }, position:'right', grid: { drawOnChartArea: false}}, 
            }
          }
      });


    }
  });
} 
 
$(document).ready(function() {
  load_sensors();
  setTimeout(request_data, 200);
});
</script>

</body>
</html>